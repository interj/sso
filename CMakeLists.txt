#USAGE:
#mkdir build; cd build
#cmake ..
#make target

cmake_minimum_required(VERSION 2.6)
project(sso)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -ansi -Wall -pthread")
set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -ansi -Wall -pthread")
set(CMAKE_BUILD_TYPE "Debug")

add_executable(minishell zad2.c)

#target fifo consists of binaries fifo_send & fifo_recv that should be run in parallel
add_custom_target(fifo)
add_executable(fifo_send fifo_send.c)
add_executable(fifo_recv fifo_recv.c)
add_dependencies(fifo fifo_send fifo_recv)

add_executable(mplayer_frontend mplayer_frontend.c)

#target dining_philosophers should be executed by starting build/shared_mem binary
add_custom_target(dining_philosophers)
add_executable(philosopher philosopher.c)
add_executable(shared_mem shared_mem.c)
add_dependencies(dining_philosophers shared_mem philosopher)

add_executable(threads threads.c)

#target socket consists of binaries socket_send & socket_recv that should be run in parallel
add_custom_target(socket)
add_executable(socket_send socket_send.c)
add_executable(socket_recv socket_recv.c)
add_dependencies(socket socket_send socket_recv)

install(TARGETS minishell mplayer_frontend threads RUNTIME DESTINATION bin)
