#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <malloc.h>
#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>
#include <fcntl.h>

bool running = true;
char* fileName = "/dev/shm/named_pipe";

void shutdown(int signal)
{
    running = false;
    printf("\ngraceful shutdown\n");
    unlink(fileName);
}

void sendCommand(char command, int file)
{
    char buffer[1024];
    switch(command)
    {
        case 'p': sprintf(buffer, "pause\n");break;
        case 'q': sprintf(buffer, "quit\n");break;
        case '+': sprintf(buffer, "seek 5\n");break;
        case '-': sprintf(buffer, "seek -5\n");break;
    }
    write(file, buffer, strlen(buffer));
}

int main(int argc, char** argv)
{
    signal(SIGINT, &shutdown);
    if(argc > 1)
        fileName = argv[1];
    if(mkfifo(fileName, 0666))
        perror("");

    int output = open(fileName, O_WRONLY);
    if(output < 0)
        perror("open failed");

    while(running)
    {
        char command = getchar();
        sendCommand(command, output);
    }
    close(output);

    return 0;
}
