#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <malloc.h>
#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>
#include <fcntl.h>

struct msg
{
    int32_t id;
    char msg[1020];
};

bool running = true;

void shutdown(int signal)
{
    running = false;
    printf("\ngraceful shutdown\n");
}

int main(int argc, char** argv)
{
    signal(SIGINT, &shutdown);
    int input = open("/dev/shm/named_pipe", O_RDONLY);
    if(input < 0)
        perror("open failed");
    int delay = 0;
    if(argc > 1)
        delay = atoi(argv[1]);

    while(running)
    {
        struct msg data;

        if(read(input, &data, sizeof(data)) == 0);
            printf("received %d KiBs of data\r", data.id);
        sleep(delay);
    }
    close(input);

    return 0;
}
