#define _XOPEN_SOURCE 700
#define _DEFAULT_SOURCE
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>
#include <signal.h>
#include <assert.h>

long monie$$$ = 500;
bool running = true;
pthread_cond_t cond_var;
pthread_mutex_t mutex;

void shutdown(int signal)
{
    running = false;
    printf("\ngraceful shutdown\n");
}

void addMoney(long addedMoney)
{
    pthread_mutex_lock(&mutex);
    monie$$$ += addedMoney;
    if(monie$$$ > 0)
    {
        pthread_cond_broadcast(&cond_var);
    }
    pthread_mutex_unlock(&mutex);
}

void takeMoney(long takenMoney)
{
    pthread_mutex_lock(&mutex);
    while(takenMoney > monie$$$)
    {
        pthread_cond_wait(&cond_var, &mutex); /*atomically lock/unlock mutex*/;
    }
    monie$$$ -= takenMoney;
    pthread_mutex_unlock(&mutex);
}

void* earn(void* args)
{
    while(running)
    {
        addMoney(rand() % 200); /*rand will surely break it's internal state, it's unimportant*/
        usleep(1e6 + rand() % 1000 * 1000);
    }
    return NULL;
}

void* spend(void* args)
{
    while(running)
    {
        takeMoney(rand() % 200); /*rand will surely break it's internal state, it's unimportant*/
        usleep(1e6 + rand() % 1000 * 1000);
    }
    return NULL;
}

int main(int argc, char** argv)
{
    srand(time(0));
    signal(SIGINT, shutdown);
    pthread_t workers[3];
    pthread_t spenders[5];
    pthread_cond_init(&cond_var, NULL);
    pthread_mutex_init(&mutex, NULL);

    int i = 0;

    for(i = 0; i < sizeof(workers) / sizeof(pthread_t); ++i)
    {
        if(pthread_create(&(workers[i]), NULL, earn, NULL))
        {
            perror("Creating thread ");
            return 1;
        }
    }
    for(i = 0; i < sizeof(spenders) / sizeof(pthread_t); ++i)
    {
        if(pthread_create(&(spenders[i]), NULL, spend, NULL))
        {
            perror("Creating thread ");
            return 1;
        }
    }

    while(running)
    {
        printf("\33[2Kmoney: %ld\r", monie$$$);
        fflush(stdout);
        usleep(5e4);
    }

    pthread_mutex_destroy(&mutex);
    pthread_cond_destroy(&cond_var);

    for(i = 0; i < sizeof(spenders) / sizeof(pthread_t); ++i)
    {
        if(pthread_cancel(spenders[i]))
        {
            perror("Joining thread ");
            return 2;
        }
    }
    for(i = 0; i < sizeof(workers) / sizeof(pthread_t); ++i)
    {
        if(pthread_join(workers[i], NULL))
        {
            perror("Joining thread ");
            return 2;
        }
    }

    return 0;
}
