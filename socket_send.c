#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

int main(int argc, char* argv[])
{
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1)
    {
        printf("Could not create socket");
    }

    struct sockaddr_in server;
    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_family = AF_INET;
    server.sin_port = htons( 1337 );

    if (connect(sock, (struct sockaddr*)&server, sizeof(server)) < 0)
    {
        perror("connect failed. Error");
        return 1;
    }

    puts("Connected\n");

#define msg_size 4096
    char message[msg_size] = {0};
    long totalData = 0;
    int sent = 0;
    while (1)
    {
        if ((sent = send(sock, message, msg_size, 0)) < 0)
        {
            perror("Send failed");
            return 1;
        }
        totalData += sent;
        printf("sent %ldKiB of data\n", totalData / 1024);
    }

    close(sock);
    return 0;
}