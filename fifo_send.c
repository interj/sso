#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <malloc.h>
#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>
#include <fcntl.h>

struct msg
{
    int32_t id;
    char msg[1020];
};

bool running = true;

void shutdown(int signal)
{
    running = false;
    printf("\ngraceful shutdown\n");
    unlink("/dev/shm/named_pipe");
}

int main(int argc, char** argv)
{
    signal(SIGINT, &shutdown);
    if(mkfifo("/dev/shm/named_pipe", 0666))
        perror("");
    int output = open("/dev/shm/named_pipe", O_WRONLY);
    if(output < 0)
        perror("open failed");

    while(running)
    {
        static int id = 1;
        struct msg data;
        data.id = id;

        write(output, &data, sizeof(data));
        printf("sent %d KiBs of data\r", id++);
    }
    close(output);

    return 0;
}
