#define _XOPEN_SOURCE 700
#define _DEFAULT_SOURCE
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <malloc.h>
#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>
#include <fcntl.h>
#include <sys/sem.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <time.h>

char* mem = NULL;

void shutdown(int signal)
{
    shmdt(mem);
    exit(0);
}

int main(int argc, char** argv)
{
    signal(SIGINT, shutdown);
    int philosopherCount = atoi(argv[1]);
    int philId = atoi(argv[2]);
    int semSet = atoi(argv[3]);
    int shmId = atoi(argv[4]);
    mem = shmat(shmId, NULL, 0);
    srand(time(0) ^ philId);

    while(true)
    {
        int nextPhilId = (philId + 1) % philosopherCount;
        struct sembuf sops[4]; /* nsops increased from 2 to 4 to cause wait operation to be fully atomic */
        sops[0].sem_num = philId;        /* Operate on semaphore philId */
        sops[0].sem_op = 0;         /* Wait for value to equal 0 */
        sops[0].sem_flg = 0;

        sops[1].sem_num = philId;        /* Operate on semaphore philId */
        sops[1].sem_op = 1;         /* Increment value by 1 */
        sops[1].sem_flg = 0;

        /* Following lines were commented out to atomically lock both semaphores and queue
         * processes requests - it is similar to arbitrator solution, but due to usage of two
         * atomic semaphores doesn't introduce additional wait associated with global arbitrator.
         * Solution protects processes from deadlocking, but doesn't fully prevent starvation. */

        /*printf("philosopher %d Waits for first fork\n", philId);
        if(semop(semSet, sops, 2) != 0)
            perror("");*/

        sops[2].sem_num = nextPhilId;        /* Operate on semaphore nextPhilId */
        sops[2].sem_op = 0;         /* Wait for value to equal 0 */
        sops[2].sem_flg = 0;

        sops[3].sem_num = nextPhilId;        /* Operate on semaphore nextPhilId */
        sops[3].sem_op = 1;         /* Increment value by 1 */
        sops[3].sem_flg = 0;

        mem[philId] = 'W';
        printf("\33[2Kphilosopher %d Waits for forks\n", philId);
        if(semop(semSet, sops, 4) != 0)  /* nsops increased from 2 to 4 to make wait atomic */
        {
            char buffer[100] = {};
            sprintf(buffer, "philosopher id %d reports: ", philId);
            perror(buffer);
        }
        mem[philId] = 'E';

        sops[0].sem_num = philId;        /* Operate on semaphore philId */
        sops[0].sem_op = -1;         /* Decrement value by 1 */
        sops[0].sem_flg = 0;

        sops[1].sem_num = nextPhilId;        /* Operate on semaphore nextPhilId */
        sops[1].sem_op = -1;         /* Decrement value by 1 */
        sops[1].sem_flg = 0;

        printf("\33[2Kphilosopher %d Eats for a while\n", philId);
        usleep(2e6 + rand() % 1000 * 1000);
        mem[philId] = 'M';
        if(semop(semSet, sops, 2) != 0)
        {
            char buffer[100] = {};
            sprintf(buffer, "philosopher id %d reports: ", philId);
            perror(buffer);
        }

        printf("\33[2Kphilosopher %d Meditates for a while\n", philId);
        fflush(stdout);
        usleep(4e6 + rand() % 1000 * 1000);
    }

    return 0;
}
