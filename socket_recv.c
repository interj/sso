#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

int main(int argc, char* argv[])
{
    int sock;
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("socket fail");
        return 1;
    }

    struct sockaddr_in server;
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons( 1337 );

    if ( bind(sock, (struct sockaddr*)&server, sizeof(server)) < 0)
    {
        perror("bind fail");
        return 1;
    }

    listen(sock, SOMAXCONN);

    printf("awaiting connections...\n");

    socklen_t addr_len = sizeof(struct sockaddr_in);

    struct sockaddr client;
    int connection = accept(sock, &client, &addr_len);
    if (connection < 0)
    {
        perror("accept failed");
        return 1;
    }

#define msg_size 4096
    char client_message[msg_size] = {0};
    int read_size;
    long totalData = 0;
    while ((read_size = recv(connection, client_message, sizeof(client_message), 0)) > 0)
    {
        totalData += read_size;
        printf("Read %ldKiB of data\n", totalData / 1024);
    }

    if (read_size == 0)
    {
        printf("connection closed\n");
        fflush(stdout);
    }
    else if (read_size == -1)
    {
        perror("recv failed");
    }

    return 0;
}