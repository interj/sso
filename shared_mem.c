#define _XOPEN_SOURCE 700
#define _DEFAULT_SOURCE
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <malloc.h>
#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>
#include <fcntl.h>
#include <sys/sem.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

bool running = true;

void shutdown(int signal)
{
    running = false;
    printf("\ngraceful shutdown\n");
}

int main(int argc, char** argv)
{
    signal(SIGINT, shutdown);

    int philosopherCount = 5;
    if(argc > 1)
        philosopherCount = atoi(argv[1]);

    int semSet = semget(IPC_PRIVATE, philosopherCount, IPC_CREAT | 0660);
    unsigned short* semValues = malloc(philosopherCount * sizeof(unsigned short));
    int i = 0;
    for(i = 0; i < philosopherCount; i++)
    {
        semValues[i] = 0;
    }
    if(semctl(semSet, 0, SETALL, semValues) == -1)
        perror("");
    free(semValues);

    int shmId = shmget(IPC_PRIVATE, philosopherCount + 1, IPC_CREAT | 0660);
    char* mem = shmat(shmId, NULL, 0);
    mem[philosopherCount] = '\0';
    memset(mem, 'M', philosopherCount);
    for(i = 0; i < philosopherCount; i++)
    {
        int cpid = fork();
        if(cpid == 0)
        {
            char buffer[4][128] = {{}};
            sprintf(buffer[0], "%d", philosopherCount);
            sprintf(buffer[1], "%d", i);
            sprintf(buffer[2], "%d", semSet);
            sprintf(buffer[3], "%d", shmId);
            execl("philosopher", "philosopher", buffer[0], buffer[1], buffer[2], buffer[3], NULL);
            perror("");
        }
    }

    while(running)
    {
        /* Error free status reading isn't really important so it's not synchronized */
        printf("philosophers status: %s\r", mem);
        fflush(stdout);
        usleep(500);
    }

    shmdt(mem);
    semctl(semSet, 0, IPC_RMID);

    for(i = 0; i < philosopherCount; i++)
    {
        int status;
        wait(&status);
    }
    shmctl(shmId, IPC_RMID, NULL);

    return 0;
}
