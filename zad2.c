#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <malloc.h>
#include <stdlib.h>


int main(int argc, char** argv)
{
    int i = 0;
    int proc_count = 0;
    int readPipe = 0;
    int forwardPipe[2] = {};
    char** command = malloc((argc + 1) * sizeof(char*));
    for(i = 1; i < argc; i++)
    {
        static int commandCounter = 0;
        int cpid = 0;

        if(strcmp(argv[i], "/") == 0) /*slash is is a pipe-like process separator */
        {
            close(forwardPipe[1]);
            pipe(forwardPipe);
            cpid = fork();
            if(cpid == 0)
            {
                close(forwardPipe[0]);
                dup2(forwardPipe[1], STDOUT_FILENO);
                dup2(readPipe, STDIN_FILENO);
                execvp(command[0], command);
            }
            readPipe = forwardPipe[0];
            proc_count++;
            commandCounter = 0;
        }
        else
        {
            command[commandCounter++] = argv[i];
            command[commandCounter] = NULL;
        }

        if(argc - 1 == i) /*last process on list isn't followed by separator */
        {
            close(forwardPipe[1]);
            cpid = fork();
            if(cpid == 0)
            {
                dup2(readPipe, STDIN_FILENO);
                execvp(command[0], command);
            }
            close(readPipe);
            proc_count++;
        }

        if(cpid == -1)
        {
            perror("Can't create child!\n");
            exit(1);
        }
    }

    free(command);

    for(i = 0; i < proc_count; i++)
    {
        int status;
        wait(&status);
    }
    return 0;
}
